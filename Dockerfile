# Run build-simple-cdd environment in a docker container

# Build this docker image:
# docker build -t buildcdd .

# Run this docker image:
# docker run -v $HOME:/home/user/ -ti buildcdd

FROM debian:stretch
MAINTAINER Marco Balmer <marco@balmer.name>
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
      apt-utils

# Create user
RUN addgroup --gid 1000 user \
        && useradd -d /home/user -g user user

# Add sudo permission
RUN apt-get update && \
     apt-get -y install sudo
RUN echo "user:user" | chpasswd && adduser user sudo


# build environment
RUN apt-get update && apt-get install -y \
        qemu \
        less \
        locales-all \
	man

# Addons
RUN apt-get update && apt-get install -y \
    vim \
    simple-cdd

USER user
ENV HOME /home/user
ENV TERM xterm-256color
ENV LANG de_CH.UTF-8

CMD ["bash"]
#CMD ["/usr/bin/build-simple-cdd", "--conf",  "/home/user/profiles/x220.conf", "--dist", "stretch"]
