#!/usr/bin/make -f

PREFIX		?= .
TMPDIR		= $(PREFIX)/tmp
CONF		= t460s
DISTRO		= stretch

all: build

build: builddockerimg

run: rundockerimg

builddockerimg:
	@echo Building docker image...
	docker build -t buildcdd .

rundockerimg:
	@echo Run docker image...
	docker run -v ${HOME}:/home/user/ -ti buildcdd

test: cdd qemu

cdd:
	@echo Building cd...
	build-simple-cdd --conf profiles/$(CONF).conf --dist $(DISTRO)

qemu:
	@echo Testing in qemu...
	#build-simple-cdd --conf profiles/$(CONF).conf -q --serial-console
	#
	# Installer need more than 128M Ram. This is not configurable
	# with build-simple-cdd 0.6.5. This is a workaround:
	qemu-img create -f qcow $(PREFIX)/qemu-test.hd.img 20G
	qemu-system-x86_64 -nographic -hda $(PREFIX)/qemu-test.hd.img \
	  -cdrom $(PREFIX)/images/debian-9.0-amd64-CD-1.iso -boot once=d -m 256M -smp 4

clean:
	@echo Cleaning up files...
	rm -rf $(PREFIX)/*.img
	rm -rf $(PREFIX)/images
	rm -rf $(TMPDIR)
	@echo done.

getpreseed:
	mkdir -p $(PREFIX)/misc
	wget -T 5 http://www.debian.org/releases/$(DISTRO)/example-preseed.txt -O misc/example-$(DISTRO)-preseed.cfg
